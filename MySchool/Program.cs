using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MySchool
{
	class Program
	{
        //change the urls in windows
		static string FileStudent = (@"/Users/kristemurumae/Projects/students.txt");
		static string FileTeacher = (@"/Users/kristemurumae/Projects/teachers.txt");
		static string FileParent = (@"/Users/kristemurumae/Projects/parents.txt");
        static string FileGrades = (@"/Users/kristemurumae/Projects/grades.txt");

       

        public static void Main()
		{
           

		     var Students = File.ReadAllLines(FileStudent)
				.Select(x => x.Split(','))
                .Select(x => new { Name = x[1], IdCode = x[0], Level = x[2] })
				.ToList();

			var Teachers = File.ReadAllLines(FileTeacher)
				.Select(x => x.Split(','))
                .Select(x => new { Name = x[1], IdCode = x[0], Lesson = x[2] })
				.ToList();

			var Parents = File.ReadAllLines(FileParent)
				.Select(x => x.Split(','))
                .Select(x => new { Name = x[1], IdCode = x[0], Child = x[2] })
				.ToList();

			var Grades = File.ReadAllLines(FileGrades)
				.Select(x => x.Split(','))
				.Select(x => new { IdCode = x[0], Lesson = x[1], GradeI = x[2], GradeII = x[3], GradeIII = x[3], })
				.ToList();

   

            //entering as a parent
            Console.WriteLine("Please enter your name:");
            var parentName = Console.ReadLine();

            var thisKid = Parents.ToLookup(x => x.Name, x => x.Child);
            foreach (var x in thisKid[parentName])
            {
                foreach (var y in Students)
                {
                    if(string.Equals(x, y.IdCode))
                    {
                        Console.WriteLine($"Your child {y.Name} and going in {y.Level} \n");
                        Console.WriteLine($"In {y.Level} are studying: ");

                        var classmates = Students.ToLookup(z => z.Level, z => z.Name);
                        foreach (var z in classmates[y.Level])
                        {
                         
                            foreach (var q in Parents)
							{
                               
                                if (string.Equals(y.IdCode, q.Child))
								{

                                    Console.WriteLine($"{z} responsible parent is {q.Name} ");
								}
							}
                        }
                        Console.WriteLine("\n");
                        Console.WriteLine("Grades: ");

                        foreach (var a in Grades)
                        {
                            
                            if (string.Equals(y.IdCode, a.IdCode))
							{
								int first = int.Parse(a.GradeI);
								int second = int.Parse(a.GradeII);
                                int third = int.Parse(a.GradeIII);
                                int average = (first + second + third) / 3;
					
                                Console.WriteLine($"{a.Lesson} {a.GradeI} {a.GradeII} {a.GradeIII}, average : {average}");
                            }
                        }

                    }
                }
            }

            /*
			//entering as student
			Console.WriteLine("Please enter your name:");
			var studentName = Console.ReadLine();





			//entering as a teacher
			Console.WriteLine("Please enter your name:");
			var teacherName = Console.ReadLine();
            */

		}
	}
}


            

            //var sth = File.ReadAllLines(FileStudent);
            //var sthelse = new List<string>(sth);

            // nimekiri = sthelse.Select(x => x.Split(','))
            //    .Select(x => new { Nimi = x[1], IdKood = x[0], Klass = x[2] })
            //    .ToList()
            //    .ForEach(x => Console.WriteLine($"{x.IdKood}"));

            //Console.WriteLine(sthelse[1][0]);



            //File.ReadAllLines(FileStudent)
            //    .Select(x => x.Split(','))
            //    .Select(x => new { Nimi = x[1], IdKood = x[0], Klass = x[2] })
            //    .ToList()
            //    .ForEach(x => Console.WriteLine($"Õpilane {x.Nimi} isikukoodiga {x.IdKood} õpib klassis {x.Klass}"));

            //Console.WriteLine("\n");
            //File.ReadAllLines(FileTeacher)
            //    .Select(x => x.Split(','))
            //    .Select(x => new { Nimi = x[1], IdKood = x[0], Aine = x[2] })
            //    .ToList()
            //    .ForEach(x => Console.WriteLine($"Õpetaja {x.Nimi} isikukoodiga {x.IdKood} õpetab ainet {x.Aine}"));

            //Console.WriteLine("\n");
            //File.ReadAllLines(FileTeacher)
            //    .Select(x => x.Split(','))
            //    .Select(x => new { Nimi = x[1], IdKood = x[0], child = x[2] })
            //    .ToList();

            //Console.WriteLine("\n");
            //File.ReadAllLines(FileLessons)
            //    .Select(x => x.Split(','))
            //    .Select(x => new { Klass = x[0], mate = x[1], keemia = x[2], muus = x[3], kunst = x[4], kirj = x[5], maj = x[6], })
            //    .ToList()
            //    .ForEach(x => Console.WriteLine($"ÕpKlassis{x.Klass} õpetatavad aine on: "));

        
    
    

